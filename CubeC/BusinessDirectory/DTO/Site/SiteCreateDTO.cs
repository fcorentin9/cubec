namespace BusinessDirectory.DTO.Site;

public class SiteCreateDTO
{
    public string City { get; set; }
}