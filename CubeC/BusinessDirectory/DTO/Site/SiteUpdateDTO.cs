namespace BusinessDirectory.DTO.Site;

public interface SiteUpdateDTO
{
    public string City { get; set; }
}