using BusinessDirectory.Models;

namespace BusinessDirectory.DTO;

public class CollaboraterCreateDTO
{
    public string LastName { get; set; }
    public string FirstName { get; set; }
    public string Phone { get; set; }
    public string Cellphone { get; set; }
    public string Email { get; set; }
    public string Password { get; set; }
    public string Role { get; set; }
    public int ServiceworkId { get; set;  }
    public int SiteId { get; set;  }
    
}

