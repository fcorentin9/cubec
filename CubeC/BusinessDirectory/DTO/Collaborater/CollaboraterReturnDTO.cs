using BusinessDirectory.Models;

namespace BusinessDirectory.DTO;

public class CollaboraterReturnDTO
{
    public int Id { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }
    public string Phone { get; set; }
    public string Cellphone { get; set; }
    public string Role { get; set; }
    public string Servicework { get; set; }
    public string Site { get; set; }
    public int Id_Service { get; set; }
    
    public int Id_Site { get; set; }


    public CollaboraterReturnDTO(int id, string firstName, string lastName, string email, string phone,
        string cellphone, string servicework, string site, string role, int idService, int idSite)
    {
        Id = id;
        FirstName = firstName;
        LastName = lastName;
        Email = email;
        Phone = phone;
        Cellphone = cellphone;
        Servicework = servicework;
        Site = site;
        Role = role;
        Id_Service = idService;
        Id_Site = idSite;
    }

    public CollaboraterReturnDTO()
    {
    }
}