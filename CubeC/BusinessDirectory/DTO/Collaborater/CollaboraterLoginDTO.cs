using BusinessDirectory.Models;

namespace BusinessDirectory.DTO;

public class CollaboraterLoginDTO
{
    public string Email { get; set; }
    public string Password { get; set; }
    
}