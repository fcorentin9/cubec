namespace BusinessDirectory.DTO;

public class CollaboraterUpdateDTO
{
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }
    public string Phone { get; set; }
    public string Cellphone { get; set; }
    public int Id_Site { get; set;  }
    public int Id_Service { get; set; }
}