using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace BusinessDirectory.Models;

public class Collaborater
{
    [JsonIgnore] public int Id { get; set; }
    [Required] public string LastName { get; set; }
    [Required] public string FirstName { get; set; }
    [Required] public string Phone { get; set; }
    public string Cellphone { get; set; }
    [Required] public string Email { get; set; }
    [Required] public string Role { get; set; }
    [ForeignKey("Site")] public int Id_Site { get; set; }
    public virtual Site Site { get; set; }
    [ForeignKey("Servicework")] public int Id_Service { get; set; }
    public virtual Servicework Servicework { get; set; }
    [ForeignKey("Password")] public int Id_Password { get; set; }
    [Required] public Password Password { get; set; }

    public Collaborater(string lastName, string firstName, string phone, string cellphone, string email, string role,
        int servicework, int site, Password password)
    {
        LastName = lastName;
        FirstName = firstName;
        Phone = phone;
        Cellphone = cellphone;
        Email = email;
        Role = role;
        Id_Service = servicework;
        Id_Site = site;
        Password = password;
    }

    public Collaborater()
    {
    }
}