using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace BusinessDirectory.Models;

public class Servicework
{
    public int Id { get; set; }
    [Required] public string Type { get; set; }

    public Servicework(string type)
    {
        Type = type;
    }
}