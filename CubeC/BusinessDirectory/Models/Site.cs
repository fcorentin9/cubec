using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace BusinessDirectory.Models;

public class Site
{
    public int Id { get; set; }
    [Required] public string City { get; set; }

    public Site(string city)
    {
        City = city;
    }
}