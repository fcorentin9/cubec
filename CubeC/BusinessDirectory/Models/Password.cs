using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace BusinessDirectory.Models;

public class Password
{
    [JsonIgnore]
    public int Id { get; set; }
    [Required]
    public string PasswordCollaborater { get; set; }
}