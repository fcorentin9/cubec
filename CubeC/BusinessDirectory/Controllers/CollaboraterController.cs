using System.Security.Claims;
using BusinessDirectory.DTO;
using BusinessDirectory.Models;
using BusinessDirectory.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace BusinessDirectory.Controllers;

[ApiController]
[Route("[controller]")]
public class CollaboraterController : ControllerBase
{
    private readonly IServiceCollaborater _service;

    public CollaboraterController(IServiceCollaborater service)
    {
        _service = service;
    }

    [HttpPost("Register")]
    public IActionResult Register([FromBody] CollaboraterCreateDTO newCollaborater)
    {
        _service.Register(newCollaborater);
        return Ok(new
        {
            Message = "Collaborateur créé",
        });
    }

    [HttpPost("login")]
    public IActionResult Login([FromBody] CollaboraterLoginDTO newCollaborater)
    {
        String jwt = _service.Login(newCollaborater);

        Collaborater collaborater = _service.GetCollaboraterByEmail(newCollaborater.Email);

        if (jwt != null)
        {
            return Ok(new
            {
                Message = "Connection effectué",
                Token = jwt,
                Role = collaborater.Role
            });
        }

        return NotFound(new
        {
            Message = "Collaborateur incorect ou mauvais mot de passe"
        });
    }
    
    [HttpPatch("patch/{id:int}")]
    public IActionResult Update(int id, [FromBody] CollaboraterUpdateDTO newCollaborater)
    {
        var found = _service.GetCollaboraterById(id);
        if (found == null)
        {
            return NotFound("Pas de collaborateur");
        }

        return Ok(_service.Update(newCollaborater, found));
    }
    
    [Authorize]
    [HttpDelete("delete/{id:int}")]
    public IActionResult Delete(int id)
    {
        _service.Delete(id);
        return Ok(new
        {
            Message = "Ok"
        });
    }

    [HttpGet("get/{id:int}")]
    public IActionResult Get(int id)
    {
        return Ok(_service.GetCollaboraterById(id));
    }

    [HttpGet("get/all")]
    public IActionResult GetAllCollaborater()
    {
        return Ok(_service.GetAllCollaborater());
    }
}