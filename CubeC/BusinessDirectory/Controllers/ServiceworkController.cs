using BusinessDirectory.Models;
using BusinessDirectory.Services;
using Microsoft.AspNetCore.Mvc;

namespace BusinessDirectory.Controllers;
[ApiController]
[Route("[controller]")]
public class ServiceworkController : ControllerBase
{
    private readonly IServiceServicework _service;

    public ServiceworkController(IServiceServicework service)
    {
        _service = service;
    }
    
    
    [HttpPost("Create")]
    public IActionResult Post([FromBody] Servicework dto)
    {
        try
        {
            Servicework servicework = new Servicework(dto.Type);
            _service.Create(servicework);
            return Created(nameof(Post), "Site created success");
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            return BadRequest($"Erreur lors de la création du site : {e.Message} ");
        }
    }
    
    [HttpDelete("delete/{id:int}")]
    public IActionResult Delete(int id)
    {
        _service.Delete(id);
        return Ok(new
        {
            Message = "ok"
        });
    }
    [HttpGet("get/{id:int}")]
    public IActionResult GetServiceworkById(int id)
    {
        return Ok(_service.GetServiceworkById(id));
    }
    [HttpGet("get/all")]
    public IActionResult GetAllServicework()
    {
        return Ok(_service.GetAllServicework());
    }
    [HttpPatch("patch/{id:int}")]
    public IActionResult Update(int id, [FromBody] Servicework newServicework)
    {
        var found = _service.GetServiceworkById(id);
        if (found == null)
        {
            return NotFound("Pas de site");
        }
        return Ok(_service.Update(newServicework, found)); 
    }
}