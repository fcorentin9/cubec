using BusinessDirectory.DTO.Site;
using BusinessDirectory.Models;
using BusinessDirectory.Services;
using Microsoft.AspNetCore.Mvc;

namespace BusinessDirectory.Controllers;

[ApiController]
[Route("[controller]")]
public class SiteController : ControllerBase
{
    private readonly IServiceSite _service;

    public SiteController(IServiceSite service)
    {
        _service = service;
    }

    [HttpPost("Create")]
    public IActionResult Post([FromBody] Site dto)
    {
        try
        {
            Site site = new Site(dto.City);
            _service.Create(site);
            return Created(nameof(Post), "Site created success");
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            return BadRequest($"Erreur lors de la création du site : {e.Message} ");
        }
    }

    [HttpPatch("patch/{id:int}")]
    public IActionResult Update(int id, [FromBody] SiteUpdateDTO newSite)
    {
        var found = _service.GetSiteById(id);
        if (found == null)
        {
            return NotFound("Pas de site");
        }

        return Ok(_service.Update(newSite, found));
    }

    [HttpDelete("delete/{id:int}")]
    public IActionResult Delete(int id)
    {
        _service.Delete(id);
        return Ok(new
        {
            Message = "ok"
        });
    }

    [HttpGet("get/{id:int}")]
    public IActionResult Get(int id)
    {
        return Ok(_service.GetSiteById(id));
    }

    [HttpGet("get/all")]
    public IActionResult GetAllSite()
    {
        return Ok(_service.GetAllSite());
    }
}