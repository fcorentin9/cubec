using BusinessDirectory.Models;
using Microsoft.EntityFrameworkCore;

namespace BusinessDirectory.Data;

public class ApplicationDbContext : DbContext
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) 
    { }
    public DbSet<Collaborater> Collaboraters { get; set; }
    public DbSet<Password> Passwords { get; set; }
    public DbSet<Site> Sites { get; set; }
    public DbSet<Servicework> Serviceworks { get; set; }

}