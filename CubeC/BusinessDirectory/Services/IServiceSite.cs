using BusinessDirectory.DTO.Site;
using BusinessDirectory.Models;

namespace BusinessDirectory.Services;

public interface IServiceSite
{
    bool Create(Site entity);
    bool Delete(int id);
    List<Site> GetAllSite();
    Site GetSiteById(int id);
    bool Update(SiteUpdateDTO newSite, Site found);
}