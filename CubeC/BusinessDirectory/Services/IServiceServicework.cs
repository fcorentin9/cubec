using BusinessDirectory.Models;

namespace BusinessDirectory.Services;

public interface IServiceServicework
{
    bool Create(Servicework entity);
    bool Delete(int id);
    List<Servicework> GetAllServicework();
    Servicework GetServiceworkById(int id);
    bool Update(Servicework newServicework, Servicework found);
}