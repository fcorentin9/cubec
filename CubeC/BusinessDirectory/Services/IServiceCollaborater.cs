using BusinessDirectory.DTO;
using BusinessDirectory.Models;

namespace BusinessDirectory.Services;

public interface IServiceCollaborater
{
    string Login(CollaboraterLoginDTO collaborater);
    bool Register(CollaboraterCreateDTO collaborater);
    bool Delete(int id);
    bool Update(CollaboraterUpdateDTO newCollaborater, Collaborater found);
    Collaborater GetCollaboraterById(int id);
    List<CollaboraterReturnDTO> GetAllCollaborater();
    Collaborater GetCollaboraterByEmail(string email);
    Collaborater GetCollaboraterByName(string name);

}