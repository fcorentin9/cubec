using BusinessDirectory.DTO.Site;
using BusinessDirectory.Models;
using BusinessDirectory.Repositories;

namespace BusinessDirectory.Services;

public class SiteService : IServiceSite
{
    private readonly IRepositorySite _repository;

    public SiteService(IRepositorySite repository)
    {
        _repository = repository;
    }

    public bool Create(Site entity)
    {
        
        _repository.Create(entity);
        return true;
    }

    public bool Delete(int id)
    {
        Site found = _repository.GetSiteById(id);
        if (found != null)
        {
            _repository.Delete(found);
            return true;
        }

        return false;
    }

    public List<Site> GetAllSite()
    {
        return _repository.GetAllSite();
    }

    public Site GetSiteById(int id)
    {
        return _repository.GetSiteById(id);
    }



    public bool Update(SiteUpdateDTO newSite, Site found)
    {
        foreach (var prop in typeof(SiteUpdateDTO).GetProperties())
        {
            if (prop.GetValue(newSite) != null)
            {
                found.GetType().GetProperty(prop.Name)?.SetValue(found, prop.GetValue(newSite));
            }
        }

        return _repository.Update(found);
    }
}