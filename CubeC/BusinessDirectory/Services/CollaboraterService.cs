using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using BusinessDirectory.DTO;
using BusinessDirectory.Models;
using BusinessDirectory.Repositories;
using Microsoft.IdentityModel.Tokens;

namespace BusinessDirectory.Services;

public class CollaboraterService : IServiceCollaborater
{
    private readonly IRepositoryCollaborater _repository;
    private String _Salt = "on veut essayer de";
    private String _Pepper = "crypter le mot de passe";
    private int _Iteration = 5;

    public CollaboraterService(IRepositoryCollaborater repository)
    {
        _repository = repository;
    }


    public bool Delete(int id)
    {
        Collaborater found = _repository.GetCollaboraterById(id);
        if (found != null)
        {
            _repository.Delete(found);
            return true;
        }

        return false;
    }

    public Collaborater GetCollaboraterById(int id)
    {
        return _repository.GetCollaboraterById(id);
    }

    public Collaborater GetCollaboraterByEmail(string email)
    {
        return _repository.GetCollaboraterByEmail(email);
    }

    public Collaborater GetCollaboraterByName(string name)
    {
        return _repository.GetCollaboraterByName(name);
    }

    public List<CollaboraterReturnDTO> GetAllCollaborater()
    {
        List<Collaborater> collaboraters = _repository.GetAllCollaboraters();

        List<CollaboraterReturnDTO> collaboraterReturnDtos = new List<CollaboraterReturnDTO>();

        foreach (var collaborater in collaboraters)
        {
           CollaboraterReturnDTO  dto = new CollaboraterReturnDTO(collaborater.Id, collaborater.FirstName, collaborater.LastName,
                collaborater.Email, collaborater.Phone, collaborater.Cellphone, collaborater.Servicework.Type,
                collaborater.Site.City, collaborater.Role, collaborater.Id_Service, collaborater.Id_Site);
            collaboraterReturnDtos.Add(dto);
        }

        return collaboraterReturnDtos;
    }

    public bool Update(CollaboraterUpdateDTO newCollaborater, Collaborater found)
    {
        foreach (var prop in typeof(CollaboraterUpdateDTO).GetProperties())
        {
            if (prop.GetValue(newCollaborater) != null)
            {
                found.GetType().GetProperty(prop.Name)?.SetValue(found, prop.GetValue(newCollaborater));
            }
        }

        return _repository.Update(found);
    }


    public string Login(CollaboraterLoginDTO collaborater)
    {
        var found = _repository.GetCollaboraterByEmail(collaborater.Email);

        if (found == null) return null;

        if (found.Password.PasswordCollaborater !=
            ComputeHash(collaborater.Password, _Salt, _Pepper, _Iteration)) return null;

        var claims = new List<Claim>()
        {
              new Claim(ClaimTypes.NameIdentifier, found.Email),
              new Claim(ClaimTypes.Role, found.Role),
        };

        return CreateToken(claims);
    }

    public bool Register(CollaboraterCreateDTO collaborater)
    {
        Password password = new Password()
        {
            PasswordCollaborater = ComputeHash(collaborater.Password, _Salt, _Pepper, _Iteration)
        };

        Collaborater newCollaborater = new Collaborater()
        {
            Email = collaborater.Email,
            FirstName = collaborater.FirstName,
            LastName = collaborater.LastName,
            Phone = collaborater.Phone,
            Cellphone = collaborater.Cellphone,
            Role = collaborater.Role,
            Id_Site= collaborater.SiteId,
            Id_Service = collaborater.ServiceworkId,
            Password = password,
        };
        _repository.Create(newCollaborater);

        return true;
    }

    public String CreateToken(IEnumerable<Claim> claims)
    {
        SymmetricSecurityKey key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(
            "Les esprits forts discutent des idées, les esprits moyens discutent des événements, les esprits faibles discutent des gens"));
        SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor()
        {
            Subject = new ClaimsIdentity(claims),
            Expires = DateTime.UtcNow.AddMinutes(30),
            Audience = "CORENTIN",
            Issuer = "CORENTIN",
            SigningCredentials = new SigningCredentials(
                key,
                SecurityAlgorithms.HmacSha256Signature
            )
        };
        JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
        SecurityToken token = tokenHandler.CreateToken(tokenDescriptor);
        return tokenHandler.WriteToken(token);
    }

    public string ComputeHash(string password, string salt, string pepper, int iteration)
    {
        if (iteration <= 0) return password;

        using var sha256 = SHA256.Create();
        var passwordSaltPepper = $"{password}{salt}{pepper}";
        var byteValue = Encoding.UTF8.GetBytes(passwordSaltPepper);
        var byteHash = sha256.ComputeHash(byteValue);
        var hash = Convert.ToBase64String(byteHash);
        return ComputeHash(hash, salt, pepper, iteration - 1);
    }
}