using BusinessDirectory.Models;
using BusinessDirectory.Repositories;

namespace BusinessDirectory.Services;

public class ServiceworkService : IServiceServicework
{
    private readonly IRepositoryServicework _repository;

    public ServiceworkService(IRepositoryServicework repository)
    {
        _repository = repository;
    }

    public bool Create(Servicework entity)
    {
        _repository.Create(entity);
        return true;
    }

    public bool Delete(int id)
    {
        Servicework found = _repository.GetServiceworkById(id);
        if (found != null)
        {
            _repository.Delete(found);
            return true;
        }

        return false;
    }

    public List<Servicework> GetAllServicework()
    {
        return _repository.GetAllServicework();
    }

    public Servicework GetServiceworkById(int id)
    {
        return _repository.GetServiceworkById(id);
    }

    public bool Update(Servicework newServicework, Servicework found)
    {
        foreach (var prop in typeof(Servicework).GetProperties())
        {
            if (prop.GetValue(newServicework) != null)
            {
                found.GetType().GetProperty(prop.Name)?.SetValue(found, prop.GetValue(newServicework));
            }
        }

        return _repository.Update(found);
    }
}