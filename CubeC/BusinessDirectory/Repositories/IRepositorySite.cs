using BusinessDirectory.Models;

namespace BusinessDirectory.Repositories;

public interface IRepositorySite
{
    Site Create(Site entity);
    bool Delete(Site entity);
    Site GetSiteById(int id);
    bool Update(Site entity);
    List<Site> GetAllSite();
}