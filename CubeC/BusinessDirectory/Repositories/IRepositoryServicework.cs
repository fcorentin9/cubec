using BusinessDirectory.Models;

namespace BusinessDirectory.Repositories;

public interface IRepositoryServicework
{
    Servicework Create(Servicework entity);
    bool Delete(Servicework entity);
    Servicework GetServiceworkById(int id);
    bool Update(Servicework entity);
    List<Servicework> GetAllServicework();
    
}