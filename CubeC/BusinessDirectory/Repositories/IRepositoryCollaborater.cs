using BusinessDirectory.Models;

namespace BusinessDirectory.Repositories;

public interface IRepositoryCollaborater
{
    Collaborater Create(Collaborater entity);
    Collaborater GetCollaboraterByEmail(string email);
    Collaborater GetCollaboraterByName(string name); 
    Collaborater GetCollaboraterById(int id);
    bool Delete(Collaborater entity);
    bool Update(Collaborater entity);
    List<Collaborater> GetAllCollaboraters(); 
    
}