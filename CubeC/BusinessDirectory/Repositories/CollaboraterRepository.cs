using BusinessDirectory.Data;
using BusinessDirectory.Models;
using Microsoft.EntityFrameworkCore;

namespace BusinessDirectory.Repositories;

public class CollaboraterRepository: BaseRepository, IRepositoryCollaborater
{
    public CollaboraterRepository(ApplicationDbContext dbContext) : base(dbContext)
    {
    }

    public Collaborater Create(Collaborater entity)
    {
        _dbContext.Collaboraters.Add(entity);
        _dbContext.SaveChanges();
        return entity; 
    }

    public Collaborater GetCollaboraterByEmail(string email)
    {
        return _dbContext.Collaboraters.Where(u => u.Email == email).Include(u => u.Password).FirstOrDefault();

    }

    public Collaborater GetCollaboraterByName(string name)
    {
        return _dbContext.Collaboraters.Where(u => u.FirstName == name).Include(u => u.Password).FirstOrDefault();

    }
    
    public Collaborater GetCollaboraterById(int id)
    {
        return _dbContext.Collaboraters.Where(u => u.Id == id).FirstOrDefault();
    }
    

    public bool Delete(Collaborater entity)
    {
        _dbContext.Collaboraters.Remove(entity);
        _dbContext.SaveChanges();
        return true; 
    }

    public bool Update(Collaborater entity)
    {
        _dbContext.Update(entity);
        _dbContext.SaveChanges();
        return true; 
    }
    
    public List<Collaborater> GetAllCollaboraters()
    {
        return _dbContext.Collaboraters.Include(c=>c.Servicework ).Include(c=>c.Site).ToList();
    }
}