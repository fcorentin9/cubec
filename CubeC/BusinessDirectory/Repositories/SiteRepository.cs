using BusinessDirectory.Data;
using BusinessDirectory.Models;

namespace BusinessDirectory.Repositories;

public class SiteRepository : BaseRepository, IRepositorySite
{
    public SiteRepository(ApplicationDbContext dbContext) : base(dbContext)
    {
    }

    public Site Create(Site entity)
    {
        _dbContext.Add(entity);
        _dbContext.SaveChanges();
        return entity;
    }

    public bool Delete(Site entity)
    {
        _dbContext.Remove(entity);
        _dbContext.SaveChanges();
        return true;
    }

    public Site GetSiteById(int id)
    {
        return _dbContext.Sites.Where(u => u.Id == id).FirstOrDefault();
    }

    public bool Update(Site entity)
    {
        _dbContext.Update(entity);
        _dbContext.SaveChanges();
        return true;
    }

    public List<Site> GetAllSite()
    {
        return _dbContext.Sites.ToList();

    }
}