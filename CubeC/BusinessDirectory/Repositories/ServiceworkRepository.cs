using BusinessDirectory.Data;
using BusinessDirectory.Models;

namespace BusinessDirectory.Repositories;

public class ServiceworkRepository : BaseRepository, IRepositoryServicework
{
    public ServiceworkRepository(ApplicationDbContext dbContext) : base(dbContext)
    {
    }

    public Servicework Create(Servicework entity)
    {
        _dbContext.Add(entity);
        _dbContext.SaveChanges();
        return entity;
    }

    public bool Delete(Servicework entity)
    {
        _dbContext.Remove(entity);
        _dbContext.SaveChanges();
        return true;
    }

    public Servicework GetServiceworkById(int id)
    {
        return _dbContext.Serviceworks.Where(u => u.Id == id).FirstOrDefault();
    }

    public bool Update(Servicework entity)
    {
        _dbContext.Update(entity);
        _dbContext.SaveChanges();
        return true;
    }

    public List<Servicework> GetAllServicework()
    {
        return _dbContext.Serviceworks.ToList();

    }
}