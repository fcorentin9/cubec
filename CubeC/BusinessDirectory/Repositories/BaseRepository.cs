using BusinessDirectory.Data;

namespace BusinessDirectory.Repositories;

public abstract class BaseRepository
{
    protected readonly ApplicationDbContext _dbContext;

    public BaseRepository(ApplicationDbContext dbContext)
    {
        _dbContext = dbContext; 
    }
}