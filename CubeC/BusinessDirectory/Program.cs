using System.Text;
using BusinessDirectory.Data;
using BusinessDirectory.Repositories;
using BusinessDirectory.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;

// Initialiation du .env
DotNetEnv.Env.Load();

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddCors(
    option =>
    {
        option.AddPolicy("allConnection",
            policyBuilder => { policyBuilder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader(); });
    });

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddScoped<IRepositoryCollaborater, CollaboraterRepository>();
builder.Services.AddScoped<IServiceCollaborater, CollaboraterService>();
builder.Services.AddScoped<IRepositorySite, SiteRepository>();
builder.Services.AddScoped<IServiceSite, SiteService>();
builder.Services.AddScoped<IRepositoryServicework, ServiceworkRepository>();
builder.Services.AddScoped<IServiceServicework, ServiceworkService>();

string connectionString = Environment.GetEnvironmentVariable("CONNECTION_STRING");
builder.Services.AddDbContext<ApplicationDbContext>(option => option.UseMySQL(connectionString));

builder.Services.AddSwaggerGen(setup =>
{
    var jwtSecurityScheme = new OpenApiSecurityScheme
    {
        BearerFormat = "JWT",
        Name = "JWT Authentication",
        In = ParameterLocation.Header,
        Type = SecuritySchemeType.Http,
        Scheme = JwtBearerDefaults.AuthenticationScheme,
        Description = "Put **_ONLY_** your JWT Bearer token on textbox below!",

        Reference = new OpenApiReference
        {
            Id = JwtBearerDefaults.AuthenticationScheme,
            Type = ReferenceType.SecurityScheme
        }
    };

    setup.AddSecurityDefinition(jwtSecurityScheme.Reference.Id, jwtSecurityScheme);

    setup.AddSecurityRequirement(new OpenApiSecurityRequirement
    {
        { jwtSecurityScheme, Array.Empty<string>() }
    });
});

builder.Services.AddCors(
    option =>
    {
        option.AddPolicy("allConnection", builder => { builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader(); });
    });

builder.Services.AddAuthentication(options =>
{
    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultSignInScheme = JwtBearerDefaults.AuthenticationScheme;
}).AddJwtBearer(options =>
{
    options.TokenValidationParameters = new TokenValidationParameters
    {
        ValidateIssuerSigningKey = true,
        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(
            "Les esprits forts discutent des idées, les esprits moyens discutent des événements, les esprits faibles discutent des gens")),
        ValidateLifetime = true,
        ValidateAudience = true,
        ValidAudience = "CORENTIN",
        ValidateIssuer = true,
        ValidIssuer = "CORENTIN",
        ClockSkew = TimeSpan.Zero
    };
});

var app = builder.Build();

app.UseCors("allConnection");

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthentication();

app.UseAuthorization();


app.MapControllers() /* .RequireAuthorization() */;

app.Run();